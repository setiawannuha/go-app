FROM alpine:3.12.4

WORKDIR /opt/go-app

COPY ./entrypoint.sh ./entrypoint.sh
COPY ./.env ./.env
COPY ./out/exampleservice ./exampleservice
COPY migration/ ./migration/

RUN chmod 775 ./entrypoint.sh
RUN chmod +x ./exampleservice

EXPOSE 3000

ENTRYPOINT [ "sh", "./entrypoint.sh"]