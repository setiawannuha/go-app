package tests

import (
	"testing"
)

func Sum(a, b int) int {
	return a + b
}

func TestSum(t *testing.T) {
	t.Run("example test", func(t *testing.T) {
		expectedData := 5
		result := Sum(2, 3)
		if result != expectedData {
			t.Errorf("failed")
		}
	})
}
